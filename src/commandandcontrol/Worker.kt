package commandandcontrol

import commandandcontrol.packets.GenericPacket
import commandandcontrol.packets.PacketType
import commandandcontrol.packets.WorkerAvailablePacket
import java.net.DatagramSocket
import java.net.InetAddress

class Worker(val name: String): Node(DatagramSocket(50000), DatagramSocket(50001)) {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val worker = Worker(InetAddress.getLocalHost().hostName)
            worker.sendAvailable()
            worker.listen()
        }
    }

    override fun onReceipt(genericPacket: GenericPacket, sourceAddress: InetAddress) {
        when (val type = genericPacket.type) {
            PacketType.WORK_DESCRIPTION -> {
                println(genericPacket.message) // do the work (i.e. print the message string)
            }
            else -> println("Worker received wrong packet type $type")
        }
    }

    /**
     * Sends Worker Available packet to Broker
     */
    private fun sendAvailable(){
        queuePacket(WorkerAvailablePacket(),"broker")
        sendPackets("broker",50001)
    }

}