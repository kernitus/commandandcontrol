package commandandcontrol.packets

enum class PacketType(val id: Byte) {

    WORK_DESCRIPTION(0), ACKNOWLEDGEMENT(1), NEGATIVE_ACKNOWLEDGEMENT(2), WORKER_UNAVAILABLE(3),
    WORKER_AVAILABLE(4), COUNTED_WORK_DESCRIPTION(5);

    companion object {
        fun fromId(id: Byte): PacketType {
            for (value in values())
                if (value.id == id)
                    return value

            //println("DID NOT MATCH TYPE! ASSUMING NAK Byte: $id")
            return NEGATIVE_ACKNOWLEDGEMENT
        }
    }
}