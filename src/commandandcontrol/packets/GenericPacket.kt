package commandandcontrol.packets

import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.nio.ByteBuffer
import kotlin.properties.Delegates

open class GenericPacket(val type: PacketType, val message: String, val extras: Array<Byte> = arrayOf()) {

    var frameId by Delegates.notNull<Byte>()
    private var packetSize: Int = 0

    // Generic packet layout:
    // PacketType - Frame ID - message length - message - extras length? - extras?

    /**
     * Attempts to send the packet to the specified destination
     */
    fun send(destinationHost: String, destinationPort: Int, sourceSocket: DatagramSocket, frameId: Byte) =
            send(InetSocketAddress(destinationHost, destinationPort), sourceSocket, frameId)

    /**
     * Send
     */
    private fun send(socketAddress: SocketAddress, sourceSocket: DatagramSocket, frameId: Byte){
            val packet = DatagramPacket(craftPacket(frameId), packetSize, socketAddress)

            println("Sending $type packet to $socketAddress id $frameId")

            try {
                sourceSocket.send(packet)
            } catch (e: IOException) {
                e.printStackTrace()
            }
    }

    /**
     * Creates the packet as a ByteArray from given components
     */
    private fun craftPacket(frameId: Byte): ByteArray {
        // Create new byte buffer of variable size
        packetSize = 3 + message.length // type + frame ID + message length, + message itself
        if(extras.isNotEmpty()) packetSize += 1 + extras.size // if any extras, extras size + extras

        val byteBuffer = ByteBuffer.allocate(packetSize)

        byteBuffer.put(type.id) // packet type
        this.frameId = frameId
        byteBuffer.put(frameId) // frame ID
        byteBuffer.put(message.length.toByte()) // message length
        byteBuffer.put(message.toByteArray()) // message

        if(extras.isNotEmpty()) {
            byteBuffer.put(extras.size.toByte())
            extras.forEach { byte -> byteBuffer.put(byte) }
        }

        return byteBuffer.array()
    }

    companion object {
        /**
         * Takes a received datagram packet and returns a readable and sendable Packet of correct type
         */
        fun reconstruct(datagramPacket: DatagramPacket): GenericPacket {
            // PacketType - frame ID - message length - message - extras length? - extras?
            val buffer = ByteBuffer.wrap(datagramPacket.data)

            val type = PacketType.fromId(buffer.get()) // Packet Type
            val frameId = buffer.get() // frame ID

            val message = getStringFromByteBuffer(buffer, buffer.get().toInt())

            if(buffer.remaining() != 0){ // There are some extras
                val extrasLength = buffer.get().toInt()
                val currentIndex: Int = buffer.position()

                val packet = GenericPacket(type, message, buffer.array().copyOfRange(currentIndex, currentIndex + extrasLength).toTypedArray())
                packet.frameId = frameId
                return packet
            }

            return GenericPacket(type, message)
        }

        /**
         * Grabs a string from inside a byte buffer.
         * @param buffer Buffer must already point to start of string
         * @param length The length of the string
         */
        private fun getStringFromByteBuffer(buffer: ByteBuffer, length: Int) : String {
            val builder = StringBuilder()
            for(i in 0 until length){
                val byte = buffer.get()
                builder.append(byte.toChar())
            }
            return builder.toString()
        }
    }
}