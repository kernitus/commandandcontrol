package commandandcontrol.packets

class AcknowledgementPacket : GenericPacket(PacketType.ACKNOWLEDGEMENT,"ACK")
class WorkerAvailablePacket: GenericPacket(PacketType.WORKER_AVAILABLE, "Available")
class WorkDescriptionPacket (message: String) : GenericPacket(PacketType.WORK_DESCRIPTION, message)