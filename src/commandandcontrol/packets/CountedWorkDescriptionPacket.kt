package commandandcontrol.packets

class CountedWorkDescriptionPacket(val repeatTimes: Byte, message: String) :
        GenericPacket(PacketType.COUNTED_WORK_DESCRIPTION, message, arrayOf(repeatTimes)) {

    constructor(genericPacket: GenericPacket):
            this(genericPacket.extras[0], genericPacket.message)

}