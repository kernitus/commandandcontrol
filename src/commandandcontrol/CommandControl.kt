package commandandcontrol

import commandandcontrol.packets.CountedWorkDescriptionPacket
import commandandcontrol.packets.GenericPacket
import java.net.DatagramSocket
import java.net.InetAddress

class CommandControl: Node(DatagramSocket(50000), DatagramSocket(50001)) {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val cc = CommandControl()
            cc.listen()

            while(true) {
                println("Please enter string to print out:")
                val message = readLine()
                println("Please enter amount of times work is to be executed:")
                val times = Integer.valueOf(readLine()).toByte()

                if(message == null) continue // input was invalid

                val packet = CountedWorkDescriptionPacket(times, message)
                cc.queuePacket(packet, "broker")
                cc.sendPackets("broker",50001)
                println("Sent $message x$times")
            }
        }
    }

    override fun onReceipt(genericPacket: GenericPacket, sourceAddress: InetAddress) {}

}