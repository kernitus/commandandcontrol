package commandandcontrol

import commandandcontrol.packets.CountedWorkDescriptionPacket
import commandandcontrol.packets.GenericPacket
import commandandcontrol.packets.PacketType
import commandandcontrol.packets.WorkDescriptionPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.*

class Broker : Node(DatagramSocket(50000), DatagramSocket(50001)) {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val broker = Broker()
            broker.listen()
        }
    }

    private val jobQueue: ArrayDeque<WorkDescriptionPacket> = ArrayDeque()
    private val workersList: LinkedList<String> = LinkedList() // worker hostname

    override fun onReceipt(genericPacket: GenericPacket, sourceAddress: InetAddress) {
        when (genericPacket.type) {
            PacketType.COUNTED_WORK_DESCRIPTION -> {
                val countedWorkDescriptionPacket = CountedWorkDescriptionPacket(genericPacket)
                val workDescriptionPacket = WorkDescriptionPacket(genericPacket.message)

                // Add WDP repeatTime times to the queue of jobs
                for (i in 0 until countedWorkDescriptionPacket.repeatTimes)
                    jobQueue.add(workDescriptionPacket)

                dispatchWork()
            }
            PacketType.WORKER_AVAILABLE -> {
                workersList.add(parseHostName(sourceAddress))
                dispatchWork()
            }
            PacketType.WORKER_UNAVAILABLE -> workersList.remove(parseHostName(sourceAddress))
            else -> println("BROKER RECEIVED WRONG PACKET TYPE")
        }
        println("Workers: " + workersList.size + " Jobs: " + jobQueue.size)
    }

    /**
     * Sends jobs (if any) to any available workers
     */
    private fun dispatchWork() {
        if(workersList.isNotEmpty()) {
            var workerIndex = 0
            var worker = workersList[workerIndex]
            // assign jobs to workers, round-robin style
            while (jobQueue.isNotEmpty()) {
                val job = jobQueue.removeFirst()
                queuePacket(job, worker)

                if(++workerIndex >= workersList.size)
                    workerIndex = 0

                worker = workersList[workerIndex]
            }

            // send all queued packets off
            for(aWorker in workersList)
                sendPackets(aWorker,50001)
        }
    }
}