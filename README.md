Project implementing distributed task assignment over a network using UDP Packets and Kotlin. A Command and Control (C&C) server sends work descriptions to a Broker, that in turn distributes the work to any available Workers. The workers can signal their availability or withdraw it. There is always one C&C and one Broker, and there can be multiple Workers. In this simple example, the work to be done is simply to print the message contained in the work description packet.

Workers only work on a single task at any given time, and as such the Broker maintains a queue of work to be done, and dispatches it to workers as they signal their availability. A work description may also contain an integer outlining how many times the task has to be repeated, which can then be spread over multiple workers to be completed in parallel.

## Packet layout

### Generic packet layout

`PacketType (Byte) - Frame ID (Byte) - Message Length (Int) - Message (String) - Extras length? (Byte integer) - Extras? (Byte array)`

### Packet types

| Id | Type  | Extras |
| --- | --- | --- |
| 0 | WORK_DESCRIPTION | Message (String) 
| 1 | ACKNOWLEDGEMENT | "ACK"
| 2 | NEGATIVE_ACKNOWLEDGEMENT |
| 3 | WORKER_UNAVAILABLE |
| 4 | WORKER_AVAILABLE | "Available"
| 5 | COUNTED_WORK_DESCRIPTION | Message (String), Repeat (Int) |

### Error-control algorithms

Each type of node (C&C, Broker, Worker) extends the Node class, which implements facilities for both sending and receiving UDP packets over the network. Both the stop-and-wait and go-back-N Automatic Repeat Request algorithms are implemented and made use of in the different types of nodes. These make sure the packets have been received correctly, and if not, are sent again until an Acknowledgment packet is received by the sender.
